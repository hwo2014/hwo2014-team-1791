﻿using System;
using System.IO;
using System.Net.Sockets;

namespace Racer
{
    public class RaceGame
    {

        public static void Main(string[] args)
        {
            string host = "testserver.helloworldopen.com";
            int port = 8091;
            string botName = "Madafakaz";
            string botKey = "xFRLY9HT09coew";
            string botType = "";
            string track = "";

            if (args.Length >= 4)
            {
                host = args[0];
                port = int.Parse(args[1]);
                botName = args[2];
                botKey = args[3];
            }

            if (args.Length >= 5)
            {
                botType = args[4].ToLower();
            }
            if (args.Length >= 6)
            {
                track = args[5].ToLower();
            }


            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                NetworkStream stream = client.GetStream();
                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                Classes.BotBase bot;
                switch (botType)
                {
                    case "marian":
                        bot = new Classes.Marian(botName, botKey);
                        Console.WriteLine("Wysylam Mariana");
                        break;
                    case "zenon":
                        bot = new Classes.Zenon(botName, botKey);
                        Console.WriteLine("Wysylam Zenona");
                        break;
                    case "stefan":
                        bot = new Classes.Stefan(botName, botKey);
                        Console.WriteLine("Wysylam Stefana");
                        break;
                    case "zoska":
                        bot = new Classes.Zoska(botName, botKey);
                        Console.WriteLine("Wysylam Zoske");
                        break;
                    default:
                        bot = new Classes.Stefan(botName, botKey);
                        Console.WriteLine("Wysylam Stefana");
                        break;
                }
                

                Handler app = new Handler(bot, reader, writer);
                app.connect(track);
                app.run();
                app.disconnect();
                
            }
            Console.ReadKey();
        }

    }
    
}