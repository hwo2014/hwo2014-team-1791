﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racer.Responses
{
    class CarPosition
    {
        private double angle = 0;

        public YourCar Id { get; set; }
        public PiecePosition PiecePosition { get; set; }
        public int Index { get; set; }
        public double Angle
        {
            get { return Math.Abs(this.angle); }
            set { this.angle = value; }
        }
        public double ActualAngle
        {
            get { return this.angle; }
            set { this.angle = value; }
        }

        public long FrameTime { get; set; }

    }

    class YourCar
    {
        public String Name { get; set; }
        public String Color { get; set; }
    }

    class IDPass
    {
        public String name { get; set; }
        public String key { get; set; }

        public IDPass(string name, string key)
        {
            this.name = name;
            this.key = key;
        }
    }

    class PiecePosition
    {
        public int PieceIndex { get; set; }
        public float InPieceDistance { get; set; }
        public Lane Lane { get; set; }
        public int Lap { get; set; }
    }
    
    class Lane
    {
        public int StartLaneIndex { get; set; }
        public int EndLaneIndex { get; set; }
    }
   
}
