﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racer.Responses
{
    class GameInit
    {
        public Race Race { get; set; }
        public RaceSession RaceSession { get; set; }
    }

    class Race
    {
        public Track Track { get; set; }
        public RaceSession RaceSession { get; set; }
    }

    class RaceSession
    {
        public int Laps { get; set; }
        public int MaxLapTimeMs { get; set; }
        public bool QuickRace { get; set; }
        public int DurationMs { get; set; }

        
    }
    
    class Track
    {
        public Pieces[] Pieces { get; set; }
        public Lanes[] Lanes { get; set; }

        public string ID { get; set; }
        public string Name { get; set; }




        public void calculateLaneLengths()
        {
            int lanesCount = this.Lanes.Length;
            int switches = 0;


            for (int _A = 0; _A < this.Pieces.Length; _A++)
            {
                this.Pieces[_A].Index = _A;
                this.Pieces[_A].LaneLength = new double[lanesCount];
                if( this.Pieces[_A].Switch) {
                    switches++;
                }

                if (_A == 0)
                {
                    this.Pieces[0].TrackAngle = 0;
                }
                else
                {
                    this.Pieces[_A].TrackAngle = Pieces[_A - 1].TrackAngle;
                }
                bool pieceReversed = false;

                if( this.Pieces[_A].TrackAngle == 90 && this.Pieces[_A].ActualAngle < 0) 
                {
                    pieceReversed = true;  
                } 
                else if( this.Pieces[_A].TrackAngle == -90 && this.Pieces[_A].ActualAngle > 0)  
                {
                    pieceReversed = false;
                } 
                else 
                {
                    if (this.Pieces[_A].TrackAngle < -90 || this.Pieces[_A].TrackAngle > 90)
                    {
                        pieceReversed = true;
                    }

                }
                this.Pieces[_A].TrackAngle += this.Pieces[_A].ActualAngle;
                // <----
                // Make sure we only work within one circle
                // ---->
                if (Math.Abs(this.Pieces[_A].TrackAngle) >= 360)
                {
                    this.Pieces[_A].TrackAngle = 0;
                }


                for (int _B = 0; _B < lanesCount; _B++)
                {
                    if (this.Pieces[_A].Angle == 0)
                    {
                        this.Pieces[_A].LaneLength[_B] = this.Pieces[_A].Length;
                    }
                    else
                    {
                   //     pieceReversed = false;
                        if (pieceReversed)
                        {
                            int tempIndex = lanesCount - 1 - _B;
                            this.Pieces[_A].LaneLength[_B] = (2 * Math.PI
                                * (this.Pieces[_A].Radius + this.Lanes[tempIndex].distanceFromCenter)
                                * (this.Pieces[_A].Angle / 360));
                        }
                        else
                        {
                            this.Pieces[_A].LaneLength[_B] = (2 * Math.PI
                                * (this.Pieces[_A].Radius + this.Lanes[_B].distanceFromCenter)
                                * (this.Pieces[_A].Angle / 360));
                        }
                    }
                }

            }
        }

        public void calculateSectionLengths()
        {
            int lanesCount = this.Lanes.Length;
            int _B = -1;

            for (int _A = 0; ; _A++)
            {
                if (_A >= this.Pieces.Length)
                {
                    _A = 0;
                }

                if (this.Pieces[_A].PreferredLane != -1)
                {
                    break;
                }

                if (this.Pieces[_A].Switch)
                {
                    if (_B != -1)
                    {
                        double maxValue = this.Pieces[_B].NextSectionLength.Min();
                        this.Pieces[_B].PreferredLane = this.Pieces[_B].NextSectionLength.ToList().IndexOf(maxValue);
                    }
                    _B = _A;
                    this.Pieces[_B].NextSectionLength = new double[lanesCount];

                    for (int _C = 0; _C < lanesCount; _C++)
                    {
                        this.Pieces[_B].NextSectionLength[_C] = 0; 
                    }

                    continue;
                }

                if (_B == -1)
                {
                    continue;
                }
                for (int _C = 0; _C < lanesCount; _C++)
                {
                    this.Pieces[_B].NextSectionLength[_C] += this.Pieces[_A].LaneLength[_C];
                }
            }

        }

        public void findTurboZones()
        {
            const int TURBO_PIECES = 6;
            const int MAX_ANGLE = 5;

            for (int _A = 0; _A < this.Pieces.Length; _A++)
            {
                if (this.Pieces[_A].Angle > 0)
                {
                    this.Pieces[_A].TurboAllowed = true;
                    for (int _B = _A + 1; _B < _A + TURBO_PIECES; _B++)
                    {
                        this.Pieces[_A].TurboAllowed = this.Pieces[_A].TurboAllowed && this.Pieces[_B % this.Pieces.Length].Angle < MAX_ANGLE;
                    }
                }
                
            }            
        }

    }

    class Pieces
    {
        private double angle = 0;


        public double Length { get; set; }
        public double[] LaneLength { get; set; }
        public double[] NextSectionLength { get; set; }
        public bool Switch { get; set; }
        public bool TurboAllowed { get; set; }
        public double Radius { get; set; }
        public int Index { get; set; }
        public int PreferredLane = -1;
        public double TrackAngle { get; set; }
        public double Angle
        {
            get { return Math.Abs(this.angle); }
            set { this.angle = value; }
        }
        public double ActualAngle
        {
            get { return this.angle; }
            set { this.angle = value; }
        }
               
    }

    class Lanes
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }
    }
}
