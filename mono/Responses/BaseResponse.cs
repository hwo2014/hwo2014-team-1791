﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racer.Responses
{
    class BaseResponse
    {
        protected long gameTick = -1;

        public string MsgType { get; set; }
        public object Data { get; set; }
        public long GameTick
        {
            get
            {
                return this.gameTick;
            }
            set
            {
                this.gameTick = value;
            }
        }
    }
}
