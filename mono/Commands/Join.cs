﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Racer.Classes;

namespace Racer.Commands
{
    class Join : BaseMessage
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}
