﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Racer.Classes;

namespace Racer.Commands
{
    class Throttle : BaseMessage
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}
