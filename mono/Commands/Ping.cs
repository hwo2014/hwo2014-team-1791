﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Racer.Classes;

namespace Racer.Commands
{
    class Ping : BaseMessage
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}
