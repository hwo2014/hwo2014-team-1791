﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racer.Commands
{
    class Switch : BaseMessage
    {      
        public string value;

        public Switch(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {

            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }

    }
}
