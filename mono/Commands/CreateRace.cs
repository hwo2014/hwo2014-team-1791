﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racer.Commands
{
    class CreateRace : BaseMessage
    {
        public Responses.IDPass botId { get; set; }
        public string trackName { get; set; }
        public int carCount { get; set; }

        public CreateRace(Responses.IDPass botId, string trackName, int carCount)
        {
            this.botId = botId;
            this.trackName = trackName;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }
}
