﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

using Racer.Classes;

namespace Racer.Commands
{
    abstract class BaseMessage
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }
        
        protected abstract string MsgType();
    }
}
