﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

using System.Diagnostics;

using Racer.Commands;
using Racer.Classes;

namespace Racer
{
    class Handler
    {
        private BotBase YOYOYO = null;
        private Responses.GameInit activeTrack = null;

        protected StreamWriter writer;
        protected StreamReader reader;

        protected bool shutdownLoops = false;
        protected string status = "";

        public Handler(BotBase bot, StreamReader reader, StreamWriter writer)
        {
            this.YOYOYO = bot;
            this.reader = reader;
            this.writer = writer;
        }

        public void connect(string track)
        {
            if (track == "")
            {
                sendCommand(new Commands.Join(YOYOYO.Name, YOYOYO.Key));
            }
            else
            {
                sendCommand(new Commands.CreateRace(new Responses.IDPass(YOYOYO.Name, YOYOYO.Key), track, 1));
            }

        }
        
        protected Commands.BaseMessage processMessage (Responses.BaseResponse msg)
        {
            switch (msg.MsgType)
            {
                case "carPositions":
                    Responses.CarPosition[] positions = JsonConvert.DeserializeObject<Responses.CarPosition[]>(msg.Data.ToString());
                    YOYOYO.setPositions(positions);
                    return YOYOYO.handleFrame();
                    break;
                case "join":
                    break;
                case "gameInit":
                    activeTrack = JsonConvert.DeserializeObject<Responses.GameInit>(msg.Data.ToString());
                    activeTrack.Race.Track.calculateLaneLengths();
                    activeTrack.Race.Track.calculateSectionLengths();
                    activeTrack.Race.Track.findTurboZones();
                    YOYOYO.Track = activeTrack.Race.Track;
                    break;
                case "gameEnd":
                    return this.YOYOYO.endRace();
                case "tournamentEnd":
                    shutdownLoops = true;
                    return this.YOYOYO.endRace();
                case "gameStart":
                    return this.YOYOYO.startRace();
                case "yourCar":
                    Responses.YourCar car = JsonConvert.DeserializeObject<Responses.YourCar>(msg.Data.ToString());
                    YOYOYO.Color = car.Color;
                    break;
                case "crash":
                    Console.WriteLine(YOYOYO.LogString);
                    Responses.YourCar crash = JsonConvert.DeserializeObject<Responses.YourCar>(msg.Data.ToString());
                    YOYOYO.crash(crash);
                    break;
                case "spawn":
                    Responses.YourCar spawn = JsonConvert.DeserializeObject<Responses.YourCar>(msg.Data.ToString());
                    return YOYOYO.spawn(spawn);
                case "turboAvailable":
                    Console.WriteLine("** Stefan, mamy turbo! **");
                    YOYOYO.TurboAvailable = true;
                    break;

                case "error":
                    Console.WriteLine("Error: " + msg.Data.ToString());
                    break;
                default:
                    Console.WriteLine("Unknown: " + msg.Data.ToString());
                    break;
            }
            return null;
        }

        public void run()
        {

            string line;
            Commands.BaseMessage nextCommand = null;

            while (!this.shutdownLoops)
            {
                line = reader.ReadLine();
                if (line == null)
                {
                    //Console.WriteLine("Empty line");
                    continue;
                }
                Responses.BaseResponse msg = JsonConvert.DeserializeObject<Responses.BaseResponse>(line);

                nextCommand = this.processMessage(msg);
                if (nextCommand == null && msg.GameTick != -1)
                {
                    nextCommand = new Commands.Ping();
                }
                if (nextCommand != null)
                {
                    if (this.status != this.YOYOYO.LogString)
                    {
                        this.status = this.YOYOYO.LogString;
                        //Console.WriteLine(this.status);
                    }
                    this.sendCommand(nextCommand);
                }
                


            }
            Console.WriteLine("Exit run loop");


        }

        public void disconnect()
        {
        }
        
        private void sendCommand(BaseMessage msg)
        {
            //Console.WriteLine("Sending: " + msg.ToJson());
            writer.WriteLine(msg.ToJson());
        }
    }
}
