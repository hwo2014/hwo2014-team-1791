﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Diagnostics;

using Racer.Commands;

namespace Racer.Classes
{
    abstract class BotBase
    {
        private Stopwatch timer = new Stopwatch();


        protected Responses.Track track = null;
        public Responses.CarPosition carPosition = null;
        public Responses.CarPosition lastCarPosition = null;
        public Responses.CarPosition newCarPosition = null;

        protected bool raceStarted = false;
        protected bool isFresh = false;
        protected double velocity = 0;

        protected string lastStatus = "";

        
        public const double MAX_SPEED = 1.0;
        public const double MIN_SPEED = 0; 


        public BotBase(string name, string key)
        {
            this.Name = name;
            this.Key = key;
            this.TurboAvailable = false;
        }

        // <----
        // Properties
        // ---->


        public string LogString { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public string Color { get; set; }
        public bool TurboAvailable { get; set; }
        public double Throttle { get; set; }
        public double Velocity
        {
            get
            {
                return this.velocity;
            }
        }
        public bool Crashed { get; set; }

        public Responses.Track Track {
            set { track = value; }
        }
               
        public Responses.Pieces getPiece(int index)
        {
            return this.track.Pieces[index % track.Pieces.Length];
        }

        public Responses.Pieces getCurrentPiece()
        {
            return this.getPiece(this.carPosition.PiecePosition.PieceIndex);
        }

        public Responses.Pieces getNextPiece()
        {
            return this.getPiece(this.carPosition.PiecePosition.PieceIndex + 1);
        }

        public Responses.Pieces getNextPiece(int count)
        {
            return this.getPiece(this.carPosition.PiecePosition.PieceIndex + count);
        }


        public void calculateVelocity()
        {
            if (this.carPosition == null || this.lastCarPosition == null || this.Crashed)
            {
                this.velocity = 0;
                return;
            }

            // <----
            // TODO
            // Expand this to take into account corners and the actual lane length
            // ---->
            double distance = 0;
            long elapsedTime = this.carPosition.FrameTime - this.lastCarPosition.FrameTime;
            
            if (this.carPosition.PiecePosition.PieceIndex == this.lastCarPosition.PiecePosition.PieceIndex)
            {
                distance = this.carPosition.PiecePosition.InPieceDistance
                    - this.lastCarPosition.PiecePosition.InPieceDistance;
            }
            else
            {
                distance = (this.track.Pieces[this.lastCarPosition.PiecePosition.PieceIndex].LaneLength[this.lastCarPosition.PiecePosition.Lane.StartLaneIndex]
                    - this.lastCarPosition.PiecePosition.InPieceDistance)
                    + this.carPosition.PiecePosition.InPieceDistance;
            }
            this.velocity = distance / elapsedTime;

        }


        public void crash(Responses.YourCar car)
        {
            this.lastCarPosition = null;
            if (car.Color == this.Color)
            {
                this.timer.Stop();
                this.Crashed = true;
                this.Throttle = 0;
                Console.WriteLine("**** gleba :( ");
            }
        }
        public Commands.BaseMessage spawn(Responses.YourCar car)
        {
            if (car.Color == this.Color)
            {
                this.Crashed = false;
                this.Throttle = 1;
                this.timer.Start();
                Console.WriteLine("Jazda, jazda, jazda jazda! ");
                return new Commands.Throttle(this.Throttle);
            }
            return null;
        }

        public Commands.BaseMessage startRace()
        {
            this.raceStarted = true;
            this.Throttle = 1;
            this.timer.Start();
            return new Commands.Throttle(this.Throttle);
        }

        public Commands.BaseMessage endRace()
        {
            this.carPosition = null;
            this.lastCarPosition = null;
            this.newCarPosition = null;
            this.raceStarted = false;
            this.Throttle = 0;
            this.timer.Stop();
            return null;
        }


        // <----
        // Determines next action based on car's position
        // ---->
        abstract public BaseMessage handleFrame();

        // <----
        // Swap positions
        // ---->
        public void swapPositions()
        {
            if (!this.isFresh)
            {
                return;
            }
            this.lastCarPosition = this.carPosition;
            this.carPosition = this.newCarPosition;
            this.newCarPosition = null;
            this.calculateVelocity();
            this.isFresh = false;
        }

        // <----
        // Loop through all cars and find Stefan
        // ---->
        public void setPositions(Responses.CarPosition[] positions)
        {

            if (this.carPosition == null || positions[this.carPosition.Index].Id.Color != this.Color)
            {
                for (int _A = 0; _A < positions.Length; _A++)
                {
                    if (positions[_A].Id.Color == this.Color)
                    {
                        positions[_A].Index = _A;
                        positions[_A].FrameTime = timer.ElapsedMilliseconds;
                        this.newCarPosition = positions[_A];
                        this.isFresh = true;
                        return;
                    }
                }

            }
            else
            {
                positions[this.carPosition.Index].Index = this.carPosition.Index;
                positions[this.carPosition.Index].FrameTime = timer.ElapsedMilliseconds;
                this.newCarPosition = positions[this.carPosition.Index];
                this.isFresh = true;
            }
        }


        public void setLog()
        {
            //Console.WriteLine("P: " + carPosition.PiecePosition.PieceIndex.ToString("00") + "; T: " + newSpeed.ToString("0.00") + "; A: " + this.carPosition.Angle.ToString("0.00") + "; V: " + velocity.ToString("0.00") + "; ");
            if (this.carPosition == null || this.Crashed)
            {
                return;
            }
            
            string status = string.Format("P: {0,3:N1}; T: {1,3:N2}; A: {2,3:N2}; V: {3,3:N2}; LS: {4,3:N1}; LE: {5,3:N1}", 
                this.carPosition.PiecePosition.PieceIndex, 
                this.Throttle, 
                this.carPosition.Angle,
                this.Velocity,
                this.carPosition.PiecePosition.Lane.StartLaneIndex,
                this.carPosition.PiecePosition.Lane.EndLaneIndex);

            this.LogString = status;
        }

    }
}
