﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Racer.Classes
{
    class MsgWrapper
    {
        public string msgType;
        public object data;

        
        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        
    }
}
