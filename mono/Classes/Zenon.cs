﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Racer.Commands;

namespace Racer.Classes
{
    class Zenon : BotBase
    {
        protected double lastThrottle = MAX_SPEED;
        protected double lastAngle = 0;

        protected bool left = true;
        protected int switchPiece = 0;

        public Zenon(string name, string key)
            : base(name, key)
        {

        }


        // <----
        // Determines next action based on car's position
        // ---->
        override public BaseMessage handleFrame()
        {
            // <----
            // Let's get a current snapshot
            // ---->
            this.swapPositions();
            // <----
            // Don't do anything if we are crashed
            // ---->
            if (this.Crashed || this.carPosition == null || this.lastCarPosition == null)
            {
                return null;
            }

            this.Throttle = this.Throttle == 0 ? 1 : 0;
            this.setLog();

            return new Commands.Throttle(this.Throttle);

        }



    }
}
