﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Racer.Classes
{
    class FindThrottle
    {
        public FindThrottle()
        { 
        }
        public double calcualte_throttle ( double roznica, double kont)
        {

            double sumThrottle = 0, throttle1 = 0, throttle2 = 0, r = 0, zmiena1 = 3, zmiena2 = 1.1;

            if (kont >= 54 || roznica >= 11)
            {
                sumThrottle = 0;
                return sumThrottle;
            }

            int a = 1,b = 10,n = 10, m = 18,i;
            double[] membershipKont1 = new double[m];
            double[] parameters1 = new double[m];
            double[] membershipKont2 = new double[n];
            double[] parameters2 = new double[n];


            for (i = 0; i < m; i++)
            {
                parameters1[i] = (Math.Exp((1 - r) * a) - 1) / (Math.Exp(a) - 1);
                r += 0.055;
            }

            r = 0;
            i = 0; 

            for (i = 0; i < n; i++)
            {
                parameters2[i] = (Math.Exp((1 - r) * b) - 1) / (Math.Exp(b) - 1);
                r += 0.1;
            }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (kont >= 0 && kont < zmiena1)
            {
                membershipKont1[0] = Math.Max(Math.Min(1, (zmiena1 - kont) / zmiena1), 0);
            }

            if (kont > 0 && kont < zmiena1 * 2)
            {
                membershipKont1[1] = Math.Max(Math.Min(Math.Min((kont / zmiena1), 1), (zmiena1 * 2 - kont) / (zmiena1 * 2 - zmiena1)), 0);
            }

            if (kont > zmiena1 && kont < zmiena1 * 3)
            {
                membershipKont1[2] = Math.Max(Math.Min(Math.Min((kont - zmiena1) / (zmiena1 * 2 - zmiena1), 1), (zmiena1 * 3 - kont) / (zmiena1 * 3 - zmiena1 * 2)), 0);
            }

            if (kont > zmiena1 * 2 && kont < zmiena1 * 4)
            {
                membershipKont1[3] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 2) / (zmiena1 * 3 - zmiena1 * 2), 1), (zmiena1 * 4 - kont) / (zmiena1 * 4 - zmiena1 * 3)), 0);
            }

            if (kont > zmiena1 * 3 && kont < zmiena1 * 5)
            {
                membershipKont1[4] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 3) / (zmiena1 * 4 - zmiena1 * 3), 1), (zmiena1 * 5 - kont) / (zmiena1 * 5 - zmiena1 * 4)), 0);
            }

            if (kont > zmiena1 * 4 && kont < zmiena1 * 6)
            {
                membershipKont1[5] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 4) / (zmiena1 * 5 - zmiena1 * 4), 1), (zmiena1 * 6 - kont) / (zmiena1 * 6 - zmiena1 * 5)), 0);
            }

            if (kont > zmiena1 * 5 && kont < zmiena1 * 7)
            {
                membershipKont1[6] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 5) / (zmiena1 * 6 - zmiena1 * 5), 1), (zmiena1 * 7 - kont) / (zmiena1 * 7 - zmiena1 * 6)), 0);
            }

            if (kont > zmiena1 * 6 && kont < zmiena1 * 8)
            {
                membershipKont1[7] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 6) / (zmiena1 * 7 - zmiena1 * 6), 1), (zmiena1 * 8 - kont) / (zmiena1 * 8 - zmiena1 * 7)), 0);
            }

            if (kont > zmiena1 * 7 && kont < zmiena1 * 9)
            {
                membershipKont1[8] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 7) / (zmiena1 * 8 - zmiena1 * 7), 1), (zmiena1 * 9 - kont) / (zmiena1 * 9 - zmiena1 * 8)), 0);
            }

            if (kont > zmiena1 * 8 && kont < zmiena1 * 10)
            {
                membershipKont1[9] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 8) / (zmiena1 * 9 - zmiena1 * 8), 1), (zmiena1 * 10 - kont) / (zmiena1 * 10 - zmiena1 * 9)), 0);
            }

            if (kont > zmiena1 * 9 && kont < zmiena1 * 11)
            {
                membershipKont1[10] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 9) / (zmiena1 * 10 - zmiena1 * 9), 1), (zmiena1 * 11 - kont) / (zmiena1 * 11 - zmiena1 * 10)), 0);
            }

            if (kont > zmiena1 * 10 && kont < zmiena1 * 12)
            {
                membershipKont1[11] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 10) / (zmiena1 * 11 - zmiena1 * 10), 1), (zmiena1 * 12 - kont) / (zmiena1 * 12 - zmiena1 * 11)), 0);
            }

            if (kont > zmiena1 * 11 && kont < zmiena1 * 13)
            {
                membershipKont1[12] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 11) / (zmiena1 * 12 - zmiena1 * 11), 1), (zmiena1 * 13 - kont) / (zmiena1 * 13 - zmiena1 * 12)), 0);
            }

            if (kont > zmiena1 * 12 && kont < zmiena1 * 14)
            {
                membershipKont1[13] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 12) / (zmiena1 * 13 - zmiena1 * 12), 1), (zmiena1 * 14 - kont) / (zmiena1 * 14 - zmiena1 * 11)), 0);
            }

            if (kont > zmiena1 * 13 && kont < zmiena1 * 15)
            {
                membershipKont1[14] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 13) / (zmiena1 * 14 - zmiena1 * 13), 1), (zmiena1 * 15 - kont) / (zmiena1 * 15 - zmiena1 * 14)), 0);
            }

            if (kont > zmiena1 * 14 && kont < zmiena1 * 16)
            {
                membershipKont1[15] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 14) / (zmiena1 * 15 - zmiena1 * 14), 1), (zmiena1 * 16 - kont) / (zmiena1 * 16 - zmiena1 * 13)), 0);
            }

            if (kont > zmiena1 * 15 && kont < zmiena1 * 17)
            {
                membershipKont1[16] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 15) / (zmiena1 * 16 - zmiena1 * 15), 1), (zmiena1 * 17 - kont) / (zmiena1 * 17 - zmiena1 * 16)), 0);
            }

            if (kont > zmiena1 * 16 && kont < zmiena1 * 18)
            {
                membershipKont1[17] = Math.Max(Math.Min(Math.Min((kont - zmiena1 * 16) / (zmiena1 * 17 - zmiena1 * 16), 1), (zmiena1 * 18 - kont) / (zmiena1 * 18 - zmiena1 * 17)), 0);
            }

            throttle1 = (parameters1[0] * membershipKont1[0] + parameters1[1] * membershipKont1[1] + parameters1[2] * membershipKont1[2] + parameters1[3] * membershipKont1[3] + parameters1[4] * membershipKont1[4] + parameters1[5] * membershipKont1[5] + parameters1[6] * membershipKont1[6] + parameters1[7] * membershipKont1[7] + parameters1[8] * membershipKont1[8] + parameters1[9] * membershipKont1[9] + parameters1[10] * membershipKont1[10] + parameters1[11] * membershipKont1[11] + parameters1[12] * membershipKont1[12] + parameters1[13] * membershipKont1[13] + parameters1[14] * membershipKont1[14] + parameters1[15] * membershipKont1[15] + parameters1[16] * membershipKont1[16] + parameters1[17] * membershipKont1[17]) / (membershipKont1.Sum());

                if (roznica >= 0 && roznica < zmiena2)
                {
                    membershipKont2[0] = Math.Max(Math.Min(1, (zmiena2 - roznica) / zmiena2), 0);
                }

                if (roznica > 0 && roznica < zmiena2 * 2)
                {
                    membershipKont2[1] = Math.Max(Math.Min(Math.Min((roznica / zmiena2), 1), (zmiena2 * 2 - roznica) / (zmiena2 * 2 - zmiena2)), 0);
                }

                if (roznica > zmiena2 && roznica < zmiena2 * 3)
                {
                    membershipKont2[2] = Math.Max(Math.Min(Math.Min((roznica - zmiena2) / (zmiena2 * 2 - zmiena2), 1), (zmiena2 * 3 - roznica) / (zmiena2 * 3 - zmiena2 * 2)), 0);
                }

                if (roznica > zmiena2 * 2 && roznica < zmiena2 * 4)
                {
                    membershipKont2[3] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 2) / (zmiena2 * 3 - zmiena2 * 2), 1), (zmiena2 * 4 - roznica) / (zmiena2 * 4 - zmiena2 * 3)), 0);
                }

                if (roznica > zmiena2 * 3 && roznica < zmiena2 * 5)
                {
                    membershipKont2[4] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 3) / (zmiena2 * 4 - zmiena2 * 3), 1), (zmiena2 * 5 - roznica) / (zmiena2 * 5 - zmiena2 * 4)), 0);
                }

                if (roznica > zmiena2 * 4 && roznica < zmiena2 * 6)
                {
                    membershipKont2[5] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 4) / (zmiena2 * 5 - zmiena2 * 4), 1), (zmiena2 * 6 - roznica) / (zmiena2 * 6 - zmiena2 * 5)), 0);
                }

                if (roznica > zmiena2 * 5 && roznica < zmiena2 * 7)
                {
                    membershipKont2[6] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 5) / (zmiena2 * 6 - zmiena2 * 5), 1), (zmiena2 * 7 - roznica) / (zmiena2 * 7 - zmiena2 * 6)), 0);
                }

                if (roznica > zmiena2 * 6 && roznica < zmiena2 * 8)
                {
                    membershipKont2[7] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 6) / (zmiena2 * 7 - zmiena2 * 6), 1), (zmiena2 * 8 - roznica) / (zmiena2 * 8 - zmiena2 * 7)), 0);
                }

                if (roznica > zmiena2 * 7 && roznica < zmiena2 * 9)
                {
                    membershipKont2[8] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 7) / (zmiena2 * 8 - zmiena2 * 7), 1), (zmiena2 * 9 - roznica) / (zmiena2 * 9 - zmiena2 * 8)), 0);
                }

                if (roznica > zmiena2 * 8 && roznica < zmiena2 * 10)
                {
                    membershipKont2[9] = Math.Max(Math.Min(Math.Min((roznica - zmiena2 * 8) / (zmiena2 * 9 - zmiena2 * 8), 1), (zmiena2 * 10 - roznica) / (zmiena2 * 10 - zmiena2 * 9)), 0);
                }

                throttle2 = (parameters2[0] * membershipKont2[0] + parameters2[1] * membershipKont2[1] + parameters2[2] * membershipKont2[2] + parameters2[3] * membershipKont2[3] + parameters2[4] * membershipKont2[4] + parameters2[5] * membershipKont2[5] + parameters2[6] * membershipKont2[6] + parameters2[7] * membershipKont2[7] + parameters2[8] * membershipKont2[8] + parameters2[9] * membershipKont2[9]) / (membershipKont2.Sum());

                sumThrottle = throttle2 * throttle1;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (sumThrottle > 0.95)
            {
                sumThrottle = 1;
            }

            return sumThrottle;
        }
    }
}