﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

using Racer.Commands;

namespace Racer.Classes
{
    class Stefan : BotBase
    {
        protected int switchPiece = 0, flag = 0, x = 1;
        protected bool left = true;
        protected FindThrottle throttleFinder = null;

        protected double lastAngle = 0;


        public Stefan(string name, string key)
            : base(name, key)
        {
            this.throttleFinder = new FindThrottle();
        }

        // <----
        // Determines next action based on car's position
        // ---->
        override public BaseMessage handleFrame()
        {
            // <----
            // Let's get a current snapshot
            // ---->
            this.swapPositions();
            // <----
            // Don't do anything if we are crashed
            // ---->
            if (this.Crashed || this.carPosition == null || this.lastCarPosition == null)
            {
                return null;
            }

            Responses.Pieces thisPiece = this.getCurrentPiece();
            Responses.Pieces nextPiece = this.getNextPiece();

            // <----
            // Switchpiece takes priority over everything, so lets do it first
            // ---->

            if (thisPiece.Switch && thisPiece.Index != this.switchPiece)
            {
                int currentLane = this.carPosition.PiecePosition.Lane.StartLaneIndex;
                if (currentLane > thisPiece.PreferredLane)
                {
                    Console.WriteLine("Sending switch left");
                    this.switchPiece = thisPiece.Index;
                    return new Commands.Switch("Left");
                }
                else if (currentLane < thisPiece.PreferredLane)
                {
                    Console.WriteLine("Sending switch right");
                    this.switchPiece = thisPiece.Index;
                    return new Commands.Switch("Right");
                }
            }


            double newThrottle = 1;
            double distance = this.carPosition.PiecePosition.InPieceDistance / thisPiece.Length;

            double difference = 0;

            if (this.carPosition.ActualAngle > 0 && this.lastAngle > 0)
            {
                difference = this.carPosition.ActualAngle - this.lastAngle;
            }
            else if (this.carPosition.ActualAngle < 0 && this.lastAngle < 0)
            {
                difference = this.lastAngle - this.carPosition.ActualAngle;
            }
            else if (this.carPosition.ActualAngle < 0 && this.lastAngle > 0)
            {
                difference = Math.Abs(this.lastAngle) + Math.Abs(this.carPosition.ActualAngle);
            }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           
            if (thisPiece.Angle == 0 && nextPiece.Angle != 0)
            {
               newThrottle = 0.2;
            }
            else if (thisPiece.Angle != 0 && nextPiece.Angle == 0)
            {
               newThrottle = 1;
            }
            else
            {
               newThrottle = this.throttleFinder.calcualte_throttle(Math.Abs(difference),Math.Abs(this.carPosition.ActualAngle));
            }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (newThrottle > MAX_SPEED)
            {
                newThrottle = MAX_SPEED;
            }

            this.lastAngle = this.carPosition.ActualAngle;

            Throttle = newThrottle;
            this.setLog();

            //if (thisPiece.TurboAllowed && this.TurboAvailable)
            //{
            //    Console.WriteLine("**** Marian, cisne turbo!!!!!!!!!!!!!!!!!!!!!");
            //    this.TurboAvailable = false;
            //    return new Commands.Turbo();
            //}

            return new Commands.Throttle(Throttle);
        }
    }
}