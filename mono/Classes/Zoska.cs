﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Racer.Classes
{
    class Zoska : BotBase
    {
        protected int switchPiece = 0;
        protected bool left = true;
        protected FindThrottle throttleFinder = null;

        protected double lastAngle = 0;


        public Zoska(string name, string key)
            : base(name, key)
        {
            this.throttleFinder = new FindThrottle();
        }

        // <----
        // Determines next action based on car's position
        // ---->
        override public Commands.BaseMessage handleFrame()
        {
            // <----
            // Let's get a current snapshot
            // ---->
            this.swapPositions();
            // <----
            // Don't do anything if we are crashed
            // ---->
            if (this.Crashed || this.carPosition == null || this.lastCarPosition == null)
            {
                return null;
            }

            Responses.Pieces thisPiece = this.getCurrentPiece();
            Responses.Pieces nextPiece = this.getNextPiece();
            Responses.Pieces nextNextPiece = this.getNextPiece(2);

            // <----
            // Switchpiece takes priority over everything, so lets do it first
            // ---->

            if (thisPiece.Switch && thisPiece.Index != this.switchPiece)
            {
                int currentLane = this.carPosition.PiecePosition.Lane.StartLaneIndex;
                if (currentLane > thisPiece.PreferredLane)
                {
                    Console.WriteLine("Sending switch left");
                    this.switchPiece = thisPiece.Index;
                    return new Commands.Switch("Left");
                }
                else if (currentLane < thisPiece.PreferredLane)
                {
                    Console.WriteLine("Sending switch right");
                    this.switchPiece = thisPiece.Index;
                    return new Commands.Switch("Right");
                }
            }


            double newThrottle = 1;
            double resultFactor = 1;
            double distance = this.carPosition.PiecePosition.InPieceDistance / thisPiece.Length;


            double difference = 0;

            if (this.carPosition.ActualAngle > 0 && this.lastAngle > 0)
            {
                {
                    difference = this.carPosition.ActualAngle - this.lastAngle;
                }
            }
            else if (this.carPosition.ActualAngle < 0 && this.lastAngle < 0)
            {
                {
                    difference = this.lastAngle - this.carPosition.ActualAngle;
                }
            }
            else if (this.carPosition.ActualAngle < 0 && this.lastAngle > 0)
            {
                difference = Math.Abs(this.carPosition.ActualAngle) + Math.Abs(this.lastAngle);
            } 
            else if( this.carPosition.ActualAngle > 0 && this.lastAngle < 0)
            {
                difference = Math.Abs(this.carPosition.ActualAngle) + Math.Abs(this.lastAngle);  
            }

            
            if (difference != 0)
            {
                newThrottle = Math.Exp(-0.909 * Math.Abs(difference));
            }
            else
            {
                newThrottle = 1;
            }


            newThrottle *= Math.Exp(-0.004 * nextPiece.Angle);
            newThrottle *= Math.Exp(-0.003 * nextNextPiece.Angle);
            newThrottle *= Math.Exp(-0.03 * this.carPosition.Angle);


            //Console.WriteLine("Diff: " + difference);
            if (newThrottle > MAX_SPEED)
            {
                newThrottle = MAX_SPEED;
            }
            if (newThrottle < 0.005)
            {
                newThrottle = 0;
            }
            this.lastAngle = this.carPosition.ActualAngle;

            Throttle = newThrottle;
            this.setLog();
            
            if (thisPiece.TurboAllowed && this.TurboAvailable)
            {
                Console.WriteLine("**** Marian, cisne turbo!!!!!!!!!!!!!!!!!!!!!");
                this.TurboAvailable = false;
                return new Commands.Turbo();
            }

            return new Commands.Throttle(Throttle);          

        }
    }
}
