﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Racer.Commands;

namespace Racer.Classes
{
    class Marian : BotBase
    {
        protected double lastThrottle = MAX_SPEED;
        protected double lastAngle = 0;

        protected bool left = true;
        protected int switchPiece = 0;

        public Marian(string name, string key)
            : base(name, key)
        {

        }


        // <----
        // Determines next action based on car's position
        // ---->
        override public BaseMessage handleFrame()
        {
            // <----
            // Let's get a current snapshot
            // ---->
            this.swapPositions();
            // <----
            // Don't do anything if we are crashed
            // ---->
            if (this.Crashed || this.carPosition == null || this.lastCarPosition == null)
            {
                return null;
            }

            Responses.Pieces thisPiece = this.getCurrentPiece();
            Responses.Pieces nextPiece = this.getNextPiece();

            double newThrottle = MAX_SPEED;
            double velocity = this.Velocity;
            double distance = this.carPosition.PiecePosition.InPieceDistance / thisPiece.Length;

            //Console.WriteLine("Car: " + this.carPosition.PiecePosition.PieceIndex + "; Piece: " + thisPiece.Index + "; Next: " + nextPiece.Index);

            if (nextPiece.Angle != 0)
            {
                if (distance > 0.7)
                {
                    if (this.carPosition.ActualAngle <= 0 && nextPiece.ActualAngle > 0)
                    {
                        newThrottle = 0.7;
                    }
                    else if (this.carPosition.ActualAngle >= 0 && nextPiece.ActualAngle < 0)
                    {
                        newThrottle = 0.7;
                    }
                    else
                    {
                        newThrottle = 0.4;
                    }
                }
                else
                {
                    newThrottle = 0.4;
                }
            }
            else
            {
                if (distance > 0.7)
                {

                    newThrottle = MAX_SPEED;
                }
                else
                {
                    newThrottle = lastThrottle;
                }
            }


            if (this.carPosition.Angle > 10)
            {
                newThrottle = 0.1;
            } 





            
            if (newThrottle > MAX_SPEED)
            {
                newThrottle = MAX_SPEED;
            }
            

            lastThrottle = newThrottle;
            lastAngle = this.carPosition.Angle;

            this.setLog();

            if (thisPiece.Switch && thisPiece.Index != this.switchPiece)
            {
                int currentLane = this.carPosition.PiecePosition.Lane.StartLaneIndex;
                if (currentLane > thisPiece.PreferredLane)
                {
                    Console.WriteLine("Sending switch left");
                    this.switchPiece = thisPiece.Index;
                    return new Commands.Switch("Left");
                }
                else if (currentLane < thisPiece.PreferredLane)
                {
                    Console.WriteLine("Sending switch right");
                    this.switchPiece = thisPiece.Index;
                    return new Commands.Switch("Right");
                }
            }
            if (thisPiece.TurboAllowed && this.TurboAvailable)
            {
                Console.WriteLine("w00p w00p Madafakaz!");
                this.TurboAvailable = false;
                return new Commands.Turbo();
            }
            if (newThrottle != Throttle)
            {
                Throttle = newThrottle;
                return new Commands.Throttle(Throttle);
            }
            return null;

        }



    }
}
